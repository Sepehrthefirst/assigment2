/**
 * author Sepehr Raissian
 * 
 * In this program I will demonstrate how to prompt the user to enter 5 different digits.
 * 
 * Pseudocode:
 * 1. I need to print a sentence so the user knows what I want from him/her.
 * 2. Create variables that stores users digits.
 * 3. Right an algorithm that calculates smallest, largest and average
 * 4. To make it slight easier I have used for loop and arrays.(not necessary but makes the job easier and the code looks neater.)
 */
import java.util.Scanner;
public class Assigment_2 {
	
 public static void main(String[] args)
	{
		double [] numbers = new double[5];

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter 5 digit and I will represent them in order of average and sum...\n");
		double smallest = Double.MAX_VALUE;
		double largest= Double.MIN_VALUE;
		
		double sum=0;
		
		for(int i=0;i<5;i++){
			System.out.print("Enter your digit here: ");
			numbers[i]= keyboard.nextDouble();
			sum=numbers[i]+sum;}
		for(int i=0;i<5;i++){
			if(smallest>numbers[i])
			{
				smallest=numbers[i];
			}
			if(largest<numbers[i])
			{	
				largest=numbers[i];
			}
			
			
		
		}
		System.out.println("");
		System.out.println("Your sum is: "+sum);
		System.out.println("Your average is: "+sum/5);
		System.out.println("your largest is: "+largest);
		System.out.println("your smallest is: "+smallest);
		
	}

}